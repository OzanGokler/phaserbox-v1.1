﻿var Controller = function () { };

Controller.prototype = {
    myGamepad: null,
    buttonUp_isDown: null,
    buttonDown_isDown: null,
    buttonLeft_isDown: null,
    buttonRight_isDown: null,
    buttonA_isDown: null,
    buttonX_isDown: null,
    buttonY_isDown: null,
    buttonB_isDown: null,
    buttonStart_isDown: null,
    buttonSelect_isDown: null,

    preload: function ()
    {
        //GAMEPAD INITIALIZE
        this.myGamepad = game.input.gamepad.pad1;
        game.input.gamepad.start();
    },

    create: function ()
    {

    },

    update: function ()
    {
        this.setButtonsFalse();
        this.buttonLinuxDetected();
        //this.buttonsDetected();
        //this.buttonsDetectedKeyboard();
    },

    setButtonsFalse: function ()
    {
        this.buttonUp_isDown = false;
        this.buttonDown_isDown = false;
        this.buttonLeft_isDown = false;
        this.buttonRight_isDown = false;
        this.buttonA_isDown = false;
        this.buttonX_isDown = false;
        this.buttonY_isDown = false;
        this.buttonB_isDown = false;
        this.buttonStart_isDown = false;
        this.buttonSelect_isDown = false;
    },

    buttonsDetected: function ()
    {
        //Set Buttons Values
        this.buttonLeft_isDown = this.myGamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT);
        this.buttonRight_isDown = this.myGamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT);
        this.buttonUp_isDown = this.myGamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_UP);
        this.buttonDown_isDown = this.myGamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_DOWN);
        this.buttonStart_isDown = this.myGamepad.isDown(Phaser.Gamepad.BUTTON_9);
        this.buttonSelect_isDown = this.myGamepad.isDown(Phaser.Gamepad.BUTTON_8);
        this.buttonA_isDown = this.myGamepad.isDown(Phaser.Gamepad.BUTTON_0);
        this.buttonX_isDown = this.myGamepad.isDown(Phaser.Gamepad.BUTTON_2);
        this.buttonY_isDown = this.myGamepad.isDown(Phaser.Gamepad.BUTTON_3);;
        this.buttonB_isDown = this.myGamepad.isDown(Phaser.Gamepad.BUTTON_1);
    },
    buttonLinuxDetected: function ()
    {
        if (this.myGamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || this.myGamepad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1)
        {
            this.buttonLeft_isDown = true;
        }
        else if (this.myGamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || this.myGamepad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1)
        {
            this.buttonRight_isDown = true;
        }

        if (this.myGamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_UP) || this.myGamepad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y) < -0.1)
        {
            this.buttonUp_isDown = true;
        }
        else if (this.myGamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_DOWN) || this.myGamepad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_Y) > 0.1)
        {
            this.buttonDown_isDown = true;
        }

        if (this.myGamepad.connected) {
            var rightStickX = this.myGamepad.axis(Phaser.Gamepad.XBOX360_STICK_RIGHT_X);
            var rightStickY = this.myGamepad.axis(Phaser.Gamepad.XBOX360_STICK_RIGHT_Y);

            if (rightStickX == -1) 
	    {
                this.buttonUp_isDown = true;
            }
	    if(rightStickX == 1)
	    {
		this.buttonDown_isDown = true;
	    } 
        }
        this.buttonStart_isDown = this.myGamepad.isDown(Phaser.Gamepad.BUTTON_9);
        this.buttonSelect_isDown = this.myGamepad.isDown(Phaser.Gamepad.BUTTON_8);
        this.buttonA_isDown = this.myGamepad.isDown(Phaser.Gamepad.BUTTON_2);
        this.buttonX_isDown = this.myGamepad.isDown(Phaser.Gamepad.BUTTON_3);
        this.buttonY_isDown = this.myGamepad.isDown(Phaser.Gamepad.BUTTON_0);
        this.buttonB_isDown = this.myGamepad.isDown(Phaser.Gamepad.BUTTON_1);
	console.log("up: " + this.buttonUp_isDown + "\n" +
		    "donw: " + this.buttonDown_isDown + "\n" +
		    "left: " + this.buttonLeft_isDown  + "\n" +
		    "right: " + this.buttonRight_isDown  + "\n" +
		    "1: " + this.buttonY_isDown  + "\n" +
		    "2: " + this.buttonB_isDown  + "\n" +
		    "3: " + this.buttonA_isDown  + "\n" +
		    "4: " + this.buttonX_isDown  + "\n");
    },

    buttonsDetectedKeyboard: function ()
    {
        //Set Buttons Values
        this.buttonStart_isDown = game.input.keyboard.isDown(Phaser.Keyboard.ENTER);
        this.buttonSelect_isDown = game.input.keyboard.isDown(Phaser.Keyboard.SHIFT);
        this.buttonUp_isDown = game.input.keyboard.isDown(Phaser.Keyboard.UP);
        this.buttonDown_isDown = game.input.keyboard.isDown(Phaser.Keyboard.DOWN);
        this.buttonLeft_isDown = game.input.keyboard.isDown(Phaser.Keyboard.LEFT);
        this.buttonRight_isDown = game.input.keyboard.isDown(Phaser.Keyboard.RIGHT);
        this.buttonA_isDown = game.input.keyboard.isDown(Phaser.Keyboard.A);
        this.buttonX_isDown = game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR);
        this.buttonY_isDown = game.input.keyboard.isDown(Phaser.Keyboard.C);
        this.buttonB_isDown = game.input.keyboard.isDown(Phaser.Keyboard.B);
    }

}

var _controller = new Controller();