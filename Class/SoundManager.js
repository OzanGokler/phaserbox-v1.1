﻿var SoundManager = function () { }

SoundManager.prototype =
{
    selectingSound: null,
    startGameSound: null,
    backSound: null,

    create: function ()
    {        
        this.startGameSound = game.add.audio("gameStartSound");
        this.selectingSound = game.add.audio("selectingSound");
        this.backSound = game.add.audio("backSound");
    }
}
var _soundManager = new SoundManager();