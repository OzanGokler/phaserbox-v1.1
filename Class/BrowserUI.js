﻿var BrowserUI = function () { };

BrowserUI.prototype = {

    boxStates: { File: 0, Games: 1},
    currBoxState : null,
    gameFileIcon: null,
    background: null,
    phaserLogo: null,
    iconsGroup: null,
    bgGroup: null,
    gameindex: null,
    gameListArray: null,
    boolTweenGame: null,
    boolUpAndDownJustPressed: null,
    boolSelectFile: null,
    boolOpenGame: null,
    boolRestart: null,

    tweenfileIcon: null,
    tweenLogo: null,

    preload: function ()
    {
       
    },

    create: function () {
        this.gameListArray = finder._gameList;
        if (finder._listPaths !== "") {
            this.currBoxState = this.boxStates.File;
        }
        this.background = game.add.sprite(0, 0, "background");
        
        this.gameFileIcon = game.add.sprite(130, game.height/2, "gameFile");
        this.gameFileIcon.anchor.setTo(0.5);
        this.tweenfileIcon = game.add.tween(this.gameFileIcon.scale).to({ x: 1.2, y: 1.2 }, 750, Phaser.Easing.Linear.In, true).loop(true);
        this.tweenfileIcon.yoyo(true);

        this.phaserLogo = game.add.sprite(game.width / 2 + 100, 60, "phaserBoxLogo");
        this.phaserLogo.anchor.setTo(0.5);
        this.tweenLogo = game.add.tween(this.phaserLogo.scale).to({ x: 1.2, y: 1.2 }, 750, Phaser.Easing.Linear.In, true).loop(true);
        this.tweenLogo.yoyo(true);
        
        this.gameindex = 0;
        

        this.boolTweenGame = false;
        this.boolUpAndDownJustPressed = false;
        this.boolSelectFile = false;
        this.boolOpenGame = false;
        this.boolRestart = false;
    },
    update: function ()
    {
        game.world.bringToTop(this.gameFileIcon);
        if (this.iconsGroup !== null) {
            game.world.bringToTop(this.iconsGroup);       
        }
        

        if (this.currBoxState === this.boxStates.File) {              
           this.FileSelect();              
        }

        if (this.currBoxState === this.boxStates.Games){
            this.gameIndexCalculator();
            this.openSelectedGame(this.gameindex);
            this.FileOut();
        }

        this.restart();
        /////////////////////

    },
    restart: function() {
        if (_controller.buttonSelect_isDown && this.boolRestart === false)
        {
            window.location.replace("file:///home/pi/Desktop/PhaserBox/index.html");
            this.gameListArray = null;
            this.boolRestart = true;
        }
        if (_controller.buttonSelect_isDown === true)
        {
            this.boolRestart = false;
        }
    },

    createIcons_and_bgs: function ()
    {
        if (this.iconsGroup !== null)
        {
            this.iconsGroup.destroy();
            this.bgGroup.destroy();
            //console.log("Boooom");
        }

        this.gameindex = 0;
        this.iconsGroup = game.add.group();
        this.iconsGroup.x = 230;
        this.iconsGroup.alpha = 0;

        this.bgGroup = game.add.group();

        for (var i = 0; i < this.gameListArray.length; i++)
        {
           
            var tempIcon = game.add.sprite(140, i * 120, "icon_" + this.gameListArray[i]);
            tempIcon.anchor.setTo(0.5);
            this.iconsGroup.add(tempIcon);

            var tempBg = game.add.sprite(0, 0, "bg_" + this.gameListArray[i]);
            tempBg.alpha = 0;
            this.bgGroup.add(tempBg);
        }
        this.iconsGroup.position.y = game.height / 2;
        
        for (var j = 0; j < this.gameListArray.length; j++)
        {
            if (j !== 0) {
                this.iconsGroup.children[j].scale.setTo(0.5);
            }
        }
    },

    gameIndexCalculator: function (positive) {
        var currY = this.iconsGroup.y;

        if (_controller.buttonDown_isDown && this.boolTweenGame === false)
        {
            if (this.gameindex >= this.gameListArray.length - 1)
            {
                this.gameindex = this.gameListArray.length - 1;
                this.boolTweenGame = false;
                game.add.tween(this.iconsGroup.children[this.gameListArray.length - 1].scale).to({ x: 1, y: 1 }, 100, Phaser.Easing.Linear.Out, true).onComplete.addOnce(this.IconsTweenCOmplete, this);
                game.add.tween(this.bgGroup.children[this.gameListArray.length - 1]).to({ alpha: 1 }, 100, Phaser.Easing.Linear.Out, true, 100);

            } else {
                this.gameindex++;
                _soundManager.selectingSound.play();
                game.add.tween(this.iconsGroup).to({ y: currY - 120 }, 300, Phaser.Easing.Bounce.Out, true).onComplete.addOnce(this.IconsTweenCOmplete, this);
            }
                     
            for (var i = 0; i < this.gameListArray.length; i++)
            {
                if (i === this.gameindex) {
                    game.add.tween(this.iconsGroup.children[i].scale).to({ x: 1, y: 1 }, 100, Phaser.Easing.Linear.Out, true);                                
                    game.add.tween(this.bgGroup.children[i]).to({ alpha: 1 }, 500, Phaser.Easing.Linear.Out, true, 500);
                } else
                {
                    game.add.tween(this.iconsGroup.children[i].scale).to({ x: 0.5, y: 0.5 }, 100, Phaser.Easing.Linear.Out, true);
                    game.add.tween(this.bgGroup.children[i]).to({ alpha: 0 }, 0, Phaser.Easing.Linear.Out, true, 0);
                }
                
            }
            this.boolTweenGame = true;
        }

        if (_controller.buttonUp_isDown && this.boolTweenGame === false)
        {
            //console.log(this.gameindex);
            if (this.gameindex <= 0) {
                this.gameindex = 0;
                this.boolTweenGame = false;
                game.add.tween(this.iconsGroup.children[0].scale).to({ x: 1, y: 1 }, 100, Phaser.Easing.Linear.Out, true).onComplete.addOnce(this.IconsTweenCOmplete, this);
                game.add.tween(this.bgGroup.children[0]).to({ alpha: 1 }, 100, Phaser.Easing.Linear.Out, true, 100);
            } else {
                this.gameindex--;
                _soundManager.selectingSound.play();
                game.add.tween(this.iconsGroup).to({ y: currY + 120 }, 300, Phaser.Easing.Bounce.Out, true).onComplete.addOnce(this.IconsTweenCOmplete, this);
            }               
            for (var k = 0; k < this.gameListArray.length; k++)
            {
                if (k === this.gameindex)
                {
                    game.add.tween(this.iconsGroup.children[k].scale).to({ x: 1, y: 1 }, 100, Phaser.Easing.Linear.Out, true);
                    game.add.tween(this.bgGroup.children[k]).to({ alpha: 1 }, 500, Phaser.Easing.Linear.Out, true, 500);                  
    
                } else
                {
                    game.add.tween(this.iconsGroup.children[k].scale).to({ x: 0.5, y: 0.5 }, 100, Phaser.Easing.Linear.Out, true);
                    game.add.tween(this.bgGroup.children[k]).to({ alpha: 0 }, 0, Phaser.Easing.Linear.Out, true, 0);
                }

            }
            this.boolTweenGame = true;
        }

    },

    IconsTweenCOmplete : function() {
        this.boolTweenGame = false;
        
    },


    FileSelect: function ()
    {
        if (_controller.buttonA_isDown && this.boolSelectFile === false)
        {
            this.currBoxState = this.boxStates.Games;
            _soundManager.startGameSound.play();
            this.createIcons_and_bgs();
            game.add.tween(this.iconsGroup).to({ alpha: 1 }, 300, Phaser.Easing.Linear.Out, true);
            game.add.tween(this.gameFileIcon).to({ alpha: 0 }, 300, Phaser.Easing.Linear.Out, true);

            game.add.tween(this.iconsGroup).to({ x: 0 }, 300, Phaser.Easing.Linear.Out, true);
            game.add.tween(this.gameFileIcon).to({ x: -80 }, 300, Phaser.Easing.Linear.Out, true);

            game.add.tween(this.bgGroup.children[0]).to({ alpha: 1 }, 500, Phaser.Easing.Linear.Out, true, 500);
            this.boolSelectFile = true;
        }
        if (_controller.buttonA_isDown === false) {
            this.boolSelectFile = false;
        }
       
    },

    FileOut: function ()
    {
        if (_controller.buttonB_isDown && this.boolSelectFile === false) {
            this.currBoxState = this.boxStates.File;
            _soundManager.backSound.play();
            game.add.tween(this.iconsGroup).to({ alpha: 0 }, 300, Phaser.Easing.Linear.Out, true);
            game.add.tween(this.gameFileIcon).to({ alpha: 1 }, 300, Phaser.Easing.Linear.Out, true);

            game.add.tween(this.iconsGroup).to({ x: 230 }, 300, Phaser.Easing.Linear.Out, true);
            game.add.tween(this.gameFileIcon).to({ x: 130 }, 300, Phaser.Easing.Linear.Out, true);

            game.add.tween(this.bgGroup).to({ alpha: 0 }, 0, Phaser.Easing.Linear.Out, true, 0);

            this.boolSelectFile = true;
        }
        if (_controller.buttonB_isDown === false) {
            this.boolSelectFile = false;
        }
        
    },

    //open game
    openSelectedGame: function (index)
    {
        //console.log("file:///media/pi/PhaserBox/" + this.gameList[index] + "/index.html");

        if (_controller.buttonStart_isDown && this.boolOpenGame === false)
        {
            window.location.replace("file:///media/pi/PHASERBOX/" + this.gameListArray[index] + "/index.html");
            //console.log("actım " +this.gameindex);
            this.boolOpenGame = true;
        }
        if (_controller.buttonStart_isDown === false)
        {
            this.boolOpenGame = false;
        }
    }
};
var _browserUI = new BrowserUI();