﻿var Loading = function () { };
Loading.prototype = {

    gameListArray: new Array(),
    gameIconPath:null,

    loadAssets: function ()
    {
        //IMAGES
        this.gameListArray = finder._gameList;
       
        for (var i = 0; i < this.gameListArray.length; i++)
        {
            var a = game.load.image("icon_" + this.gameListArray[i], "/media/pi/PHASERBOX/" + this.gameListArray[i] + "/icon_" + this.gameListArray[i] + ".png");
            var b = game.load.image("bg_" + this.gameListArray[i], "/media/pi/PHASERBOX/" + this.gameListArray[i] + "/bg_" + this.gameListArray[i] + ".png");

        }

        
        game.load.image("icon", "Assets/icon.png");
        game.load.image("gameFile", "Assets/gameFile.png");
        game.load.image("phaserBoxLogo", "Assets/phaserBoxLogo.png");
        game.load.image("background", "Assets/background.png");

        //Sounds
        game.load.audio("selectingSound", "Assets/Sounds/selecting.wav");
        game.load.audio("gameStartSound", "Assets/Sounds/startGame.wav");
        game.load.audio("backSound", "Assets/Sounds/back.wav");
    },

    loadScripts: function ()
    {
        game.load.script("GameManager", "MainClass/GameManager.js");
        game.load.script("Controller", "Class/Controller.js");
        game.load.script("Sounds", "Class/SoundManager.js");
        game.load.script("BrowserUI", "Class/BrowserUI.js");
                       
    },

    StartGameManager: function ()
    {
        game.state.add(gameStates.GameManager, _gameManager);

        game.state.start(gameStates.GameManager);
    },

    preload: function ()
    {
        this.loadAssets();
        this.loadScripts();
       
    },

    create: function ()
    {
        this.StartGameManager();
    }
};

var _loading = new Loading();
