﻿var GameManager = function () { };

GameManager.prototype = {

    preload: function () {
        _controller.preload();
        
    },

    create: function () {
        _soundManager.create();
        _browserUI.create();
    },

    update: function ()
    {
        _controller.update();
        _browserUI.update();
    }
}

var _gameManager = new GameManager();