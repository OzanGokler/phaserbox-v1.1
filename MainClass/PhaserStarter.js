﻿var gameWidth = 800;
var gameHeight = 480;
var game = new Phaser.Game(gameWidth, gameHeight, Phaser.CANVAS, "Survivor Zombie"
    /*{ preload: preload, create: create, update: update }*/);
var gameStates = {
    GameManager: "gameManager",
    Loading: "gameLoading",
};



Starter = function () { };
Starter.prototype =
{

    preload: function ()
    {
        game.load.script("Finder", "MainClass/Finder.js");
        game.load.script(gameStates.Loading, "MainClass/Loading.js");
    },
    create: function () {
        game.state.add(gameStates.Loading, Loading);


        game.state.start(gameStates.Loading);

    }
};

game.state.add("Starter", Starter);
game.state.start("Starter");
